#!/usr/bin/env python
# -*- encoding: utf8

'''
The feature detection functions used in cam_monthly_analysis.py and cam_multiyear_analysis.py

Developed for use with NorESM/NorCPM to make weather features and storm track diagnostics 
available also for simulations where storing high-temporal resolution output is not feasible.
The script and procedure might be easily transferable to any coupled model using CAM as its 
atmospheric component.

The atmospheric feature detections are tuned to give reasonable results for instantaneous 
output on a global 144 x 96 grid, corresponding to 2.5 x 1.9 degree resolution.

Feature detections and diagnostics calculations are based on ``dynlib'', a library of tools
developed in the atmospheric dynamics work group at the Geophysical Institute, University of 
Bergen. 

Output is expected as daily means or 6 hourly instantaneous values, one file per output time
step. Call the script separately for each ensemble member.

On Fram this script requires the following modules
 - Python/3.6.4-intel-2018a
 - ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
 - netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

Initial development: 
Clemens Spensberger, August-December 2019
'''

import numpy as np
import netCDF4 as nc
import cftime

from dynlib import dynfor
import dynlib.detect
import dynlib.thermodyn


dynfor.config.nsmooth = 0
dynfor.config.searchrad = 1.5
dynfor.config.grid_cyclic_ew = True


def detect_cyclones(msl, grid):
    dynfor.config.cyc_minsize = 0.5e6     # in km^2; Minimum size of cyclone mask
    dynfor.config.cyc_maxsize = 4.5e6     # in km^2; Maximum size of cyclone mask
    dynfor.config.cyc_maxoro = 1500       # in m; Maximum orographic height over which cyclone centers are detected
    dynfor.config.cyc_mindist = 750       # in km; Minimum distance between two cyclone centres
    dynfor.config.cyc_minprominence = 200 # in Pa; Minimum difference between outermost contour and SLP minimum

    return dynlib.detect.cyclone_by_contour(msl, grid)


def detect_jets(u_250, v_250, grid):
    # Minimum wind shear gradient perpendicular to the wind direction
    dynfor.config.jetint_thres = 2.233e-9  # NorCPM1 CMIP6, daymean, Reanalysis 2018
    #dynfor.config.jetint_thres = 2.617e-9  # NorESM1, MIS3b stadial, 6-hourly
    #dynfor.config.jetint_thres = 2.964e-9  # NorESM2-MM PI and HIST1980-2010, daymeans
    #dynfor.config.jetint_thres = 2.124e-9  # NorESM2-LM HIST1980-2010, daymeans

    dynfor.config.minlen = 2.0e-6          # Minimum length 2000 km

    ja, jaoff = dynlib.detect.jetaxis(3000, 200, u_250, v_250, grid.dx, grid.dy)
    jaoff = jaoff.astype('i2')

    return ja, jaoff


def detect_rwb(z_250, grid):
    # The RWB detections do not actually depend on latitude and longitude, so it's fine to inject bogus values
    #
    # Two problems can be avoided using these bogus values
    #  - rwb_by_contours reshuffles longitudes to have them always start at -180. The reshuffeling is not reversed,
    #    such that then the output grid does not correspond to the input grid
    #  - rwb_by_contour implicitly assumes that dlon==dlat, which is violated in typical NorESM output. To avoid
    #    interpolation, we here cut out the tropics (where no wave breaking occurs) and pretend the remaining grid 
    #    covering the mid and high latitudes on both hemispheres has square cells.
    
    s = z_250.shape

    dlon = grid.x[0,1] - grid.x[0,0]
    dlat = abs(grid.y[1,0] - grid.y[0,0])
    if not dlon == dlat:
        # Prepare dummy grid
        x_dummy = np.arange(-180,180,dlon)
        y_dummy = np.arange(90,-90.1,-dlon)
        ylen = len(y_dummy)
        dy_dummy = grid.dy[:ylen,:] * dlon / dlat
        
        # Cut out NH and SH mid and high latitudes and combine in one array
        z_250_cut = np.zeros((s[0], ylen, s[2]))
        z_250_cut[:,:ylen//2,:] = z_250[:,:ylen//2,:]
        z_250_cut[:,-ylen//2+1:,:] = z_250[:,-ylen//2+1:,:]
        
        # Do the actual RWB detections
        rwb_a_cut, rwb_c_cut = dynlib.detect.rwb_by_contour(z_250_cut, 'Z2', x_dummy, y_dummy, 21, grid.dx[:ylen,:], dy_dummy)

        # Put the RWB detections back on the standard grid
        rwb_a = np.zeros(s)
        rwb_c = np.zeros(s)
        rwb_a[:,:ylen//2,:] = rwb_a_cut[:,:ylen//2,:]
        rwb_c[:,:ylen//2,:] = rwb_c_cut[:,:ylen//2,:]
        rwb_a[:,-ylen//2+1:,:] = rwb_a_cut[:,-ylen//2+1:,:]
        rwb_c[:,-ylen//2+1:,:] = rwb_c_cut[:,-ylen//2+1:,:]

    else:
        x_dummy = np.arange(-180,180,dlon)
        rwb_a, rwb_c = dynlib.detect.rwb_by_contour(z_250, 'Z2', x_dummy, grid.y[:,0], 21, grid.dx, grid.dy)
    
    return rwb_a, rwb_c


def detect_block(z_250, grid, tsperday):
    min_duration = 5 * tsperday + 1  # 5 days -> time steps (including start and end)
    dynfor.config.block_dj = int(round(15.0/(grid.y[1,0]-grid.y[0,0]))) # 15 deg lat
    dynfor.config.block_di = int(round(7.5/(grid.x[0,1]-grid.x[0,0])))  # 7.5 deg lon

    # Do the actual detections (NH)
    idx0 = np.argmin(np.abs(grid.y[:,0]-30))
    idx1 = np.argmin(np.abs(grid.y[:,0]-70))
    lat0, lat1 = grid.y[idx0,0], grid.y[idx1,0]
    blockmask = dynlib.detect.block_by_grad_rev(-z_250, grid, min_duration=min_duration, 
            lat_band=(lat0, lat1))
    
    # And for the SH
    idx0 = np.argmin(np.abs(grid.y[:,0]+30))
    idx1 = np.argmin(np.abs(grid.y[:,0]+70))
    if idx0 > idx1:
        idx1, idx0 = idx0, idx1
    lat0, lat1 = grid.y[idx0,0], grid.y[idx1,0]
    blockmask_SH = dynlib.detect.block_by_grad_rev(z_250, grid, min_duration=min_duration, 
            lat_band=(lat0, lat1))

    # Merge results
    blockmask[:,idx0:idx1,:] = blockmask_SH[:,idx0:idx1,:]

    return blockmask


def detect_frontlines(t_850, q_850, u_850, v_850, grid):
    # NEW: K per 100 km of theta_E
    dynfor.config.frint_thres = 2.232e-5 # NorCPM1, daymean, Reana 2018
    #dynfor.config.frint_thres = 2.338e-5 # NorESM2-LM, daymean, HIST 1980-2010

    dynfor.config.frspd_thres = 1.5     # Minimum movement of 1.5 m/s toward cold/ warm side to count as warm/ cold front
    dynfor.config.minlen = 500.0e3      # 500 km minimum front length
    dynfor.config.nsmooth = 0           # Smoothing applied manually, because input field contains NaNs
    
    p_850 = np.ones(q_850.shape) * 850.0e2
    eqpt_850 = dynlib.thermodyn.thetae_from_q(q_850, t_850, p_850)
    eqpt_850 = dynlib.utils.smooth_xy_nan(eqpt_850, 5)
    fronts, froff = dynlib.detect.frontline_at_maxgrad(2000, 200, eqpt_850, u_850, v_850, grid.dx, grid.dy)
    froff = froff.astype('i2')

    return fronts, froff


def detect_frontalvolumes(t_700, t_850, t_925, q_700, q_850, q_925, grid):
    # K per 100 km of theta_E
    dynfor.config.tfp_mingrad_largescale = 2.221e-5   # NorCPM1, daymean, 2018
    #dynfor.config.tfp_mingrad_largescale = 2.382e-5 # NorESM2-LM, daymean, HIST 1980-2010

    dynfor.config.tfp_grad_minsize = 75000.0e6     # 75000 km^2 minimum average area on each level 
    
    # Assemble a 3d potential temperature field
    s = t_850.shape
    t_3d = np.empty((s[0],3)+s[1:])
    p = np.ones(q_700.shape) * 700.0e2
    t_3d[:,0,:,:] = dynlib.thermodyn.thetae_from_q(q_700, t_700, p)
    p[:,:,:] = 850.0e2
    t_3d[:,1,:,:] = dynlib.thermodyn.thetae_from_q(q_850, t_850, p)
    p[:,:,:] = 925.0e2
    t_3d[:,2,:,:] = dynlib.thermodyn.thetae_from_q(q_925, t_925, p)
    
    # Do the actual detection
    frovo_id = dynlib.detect.frontalvolume_largescale(t_3d, grid.dx, grid.dy)

    return frovo_id


def _bandpass(lowpass_cutoff, highpass_cutoff):
    if not lowpass_cutoff and not highpass_cutoff:
        raise ValueError('Either a lower or an upper cutoff must be given!')

    def filtr_gain(freq):
        gain = np.ones(freq.shape)
        
        # Bandpass filter: Cut high and low frequencies
        if lowpass_cutoff and highpass_cutoff:
            mask = np.logical_or(freq > 1.0/lowpass_cutoff, freq < 1.0/highpass_cutoff)
            gain[mask] = 0.0
        # Low-pass filter: Cut high frequencies
        elif lowpass_cutoff:
            gain[freq > 1.0/lowpass_cutoff] = 0.0
        # High-pass filter: Cut low frequencies
        elif highpass_cutoff:
            gain[freq < 1.0/highpass_cutoff] = 0.0
        # Should never happen.
        else:
            raise ValueError('Either a lower or an upper cutoff must be given!')

        return gain
        
    return filtr_gain


def calc_eddyvar(u_850, v_850, t_850, q_850, u_250, v_250, z_250, tsperday):
    filter_2_8 = _bandpass(lowpass_cutoff=2, highpass_cutoff=8)
    filter_8_ = _bandpass(lowpass_cutoff=8, highpass_cutoff=30)

    idat = {
        ('850', 'u'): u_850, ('850', 'v'): v_850, ('850', 't'): t_850, ('850', 'q'): q_850,
        ('250', 'u'): u_250, ('250', 'v'): v_250, ('250', 'z'): z_250,
    }
    
    sdat = {}
    for (plev, q), ivar in idat.items():
        # Fourier transform
        ivar = ivar - ivar.mean(axis=0)
        dats = np.fft.rfft(ivar, axis=0)
        datfreq = np.fft.rfftfreq(ivar.shape[0], d=1.0/tsperday)
        
        # Apply the filter in spectral space
        gain = filter_2_8(datfreq)
        l2h8s = dats * gain[:,np.newaxis,np.newaxis]
        gain = filter_8_(datfreq)
        l8s  = dats * gain[:,np.newaxis,np.newaxis]
        
        # Inverse transform
        sdat[plev,q,'l2h8'] = np.fft.irfft(l2h8s, axis=0)
        sdat[plev,q,'l8'] = np.fft.irfft(l8s, axis=0)

    combinations = [
        ('850', 'uv'), ('850', 'vt'), ('850', 'vq'), ('850', 'uu'), ('850', 'vv'), 
        ('250', 'uv'), ('250', 'zz'), ('250', 'uu'), ('250', 'vv'), 
    ]
    
    odat_l2h8 = {}
    odat_l8 = {}
    for plev, qq in combinations:
        odat_l2h8[plev,qq] = sdat[plev,qq[0],'l2h8'] * sdat[plev,qq[1],'l2h8']
        odat_l8[plev,qq] = sdat[plev,qq[0],'l8'] * sdat[plev,qq[1],'l8']
    
    return odat_l2h8, odat_l8


def save(fname, dat, grid):
    f = nc.Dataset(fname, 'w', format='NETCDF4')

    f.createDimension('lat', grid.x.shape[0])
    v = f.createVariable('lat', 'f4', ('lat',))
    v[::] = grid.y[:,0]

    f.createDimension('lon', grid.x.shape[1]) 
    v = f.createVariable('lon', 'f4', ('lon',))
    v[::] = grid.x[0,:]
    
    for plev, q in dat:
        ncvar = f'/{plev}/{q}'
        ovar = f.createVariable(ncvar, 'f4', ('lat', 'lon'))
        ovar[::] = dat[plev, q]

    f.close()


def save_time(fname, dates, dat, grid):
    f = nc.Dataset(fname, 'w', format='NETCDF4')
    
    t_unit = 'days since 1850-01-01' 
    f.createDimension('time', len(dates))
    v = f.createVariable('time', 'f8', ('time',), )
    v[::] = nc.date2num(dates, t_unit, calendar=dates[0].calendar)
    v.setncattr('unit', t_unit)
    v.setncattr('calendar', dates[0].calendar)

    f.createDimension('lat', grid.x.shape[0])
    v = f.createVariable('lat', 'f4', ('lat',))
    v[::] = grid.y[:,0]

    f.createDimension('lon', grid.x.shape[1]) 
    v = f.createVariable('lon', 'f4', ('lon',))
    v[::] = grid.x[0,:]
    
    for plev, q in dat:
        ncvar = f'/{plev}/{q}'
        ovar = f.createVariable(ncvar, 'f4', ('time', 'lat', 'lon'))
        ovar[::] = dat[plev, q]

    f.close()


# est la fin.
