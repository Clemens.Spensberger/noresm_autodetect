#!/usr/bin/env python
# -*- encoding: utf8

'''
Plot multi-year climatologies of the available diagnostics

This script is intended to be used on the same data used to determine appropriate 
detection thresholds. It can does provide a quick double-check for the determined
thresholds and whether the detection algorithms all work as expected.

On Fram this script requires the following modules
 - Python/3.6.4-intel-2018a
 - ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
 - netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

Initial development: 
Clemens Spensberger, August 2019
'''

import numpy as np
from cftime._cftime import DatetimeNoLeap as dt, timedelta as td

from dynlib.metio.generic import metopen, conf

import dynlib.interpol
import dynlib.utils

from cam_detection_functions import *



dates = [dt(1370,1,2) + td(73)*i for i in range(25)]
#basepath = '/projects/NS9560K/noresm/cases'
basepath = '/Data/gfi/spengler/csp001/noresm'
case = 'N1850frc2_f09_tn14_20191012'
member = None
output = 'h1'
tsperfile = 73
tsperday = 1
#fname_pattern = '{case}/atm/hist/{case}.cam.{output}.{yr:04d}-{month:02d}-{day:02d}-00000'
fname_pattern = '{case}.cam.{output}.{yr:04d}-{month:02d}-{day:02d}-00000'
#conf.gridsize = (96,144)
conf.gridsize = (192,288)


print(dates[0], dates[-1])


conf.datapath = [basepath, ]

dynfor.config.nsmooth = 0
dynfor.config.searchrad = 1.5
dynfor.config.grid_cyclic_ew = True


# Instantaneous diagnostics
CALC_CYC = True             # Cyclone identification after Wernli and Schwierz (2006)
CALC_JETS = True            # Jet axes after Spensberger et al. (2017)
CALC_RWB = True             # Rossby wave breaking after Rivière (2007)
CALC_FRONTLINES = True      # Front lines after Jenkner et al. (2010), Schemm et al. (2015)
CALC_FRONTVOLUMES = True    # Frontal volumes after Spensberger and Sprenger (2018)
CALC_EDDYVAR = True         # Eddy covariances u'v', v'T', u'u', v'v' and z'z' 
                            #    on 2-8 and 8-30 day windows (see Chang et al. 2002)
                            
# Diagnostics requiring tracking in time 
CALC_BLOCK = True           # Atmospheric blocks after Masato et al. (2014)


UV_250 = CALC_JETS | CALC_EDDYVAR
Z_250 = CALC_RWB | CALC_EDDYVAR | CALC_BLOCK
TQ_700 = CALC_FRONTVOLUMES
UV_850 = CALC_FRONTLINES | CALC_EDDYVAR
TQ_850 = CALC_FRONTVOLUMES | CALC_FRONTLINES | CALC_EDDYVAR
TQ_925 = CALC_FRONTVOLUMES
SLP = CALC_CYC
ORO = CALC_CYC


if __name__ == '__main__':
    s = (len(dates)*tsperfile,)+conf.gridsize

    if UV_250:
        u_250 = np.zeros(s)
        v_250 = np.zeros(s)
    if Z_250:
        z_250 = np.zeros(s)
    if TQ_700:
        t_700 = np.zeros(s)
        q_700 = np.zeros(s)
    if UV_850:
        u_850 = np.zeros(s)
        v_850 = np.zeros(s)
    if TQ_850:
        t_850 = np.zeros(s)
        q_850 = np.zeros(s)
    if TQ_925:
        t_925 = np.zeros(s)
        q_925 = np.zeros(s)
    if SLP:
        slp = np.zeros(s)
    if ORO:
        oro = np.zeros(s)
    
    tsteps = []
    for tidx, date in enumerate(dates):
        if date.day == 1 or tsperfile >= 30:
            print(f'Reading {date}.')
        
        if member:
            f, grid = metopen(fname_pattern.format(case=case, member=member, output=output, 
                yr=date.year, month=date.month, day=date.day), quiet=True)
        else:
            f, grid = metopen(fname_pattern.format(case=case, output=output, 
                yr=date.year, month=date.month, day=date.day), quiet=True)

        tsteps.extend(grid.t_parsed)
    
        # Calculate pressure on hybrid sigma pressure levels
        A = f.variables['hyam'][:][np.newaxis,:,np.newaxis,np.newaxis]
        B = f.variables['hybm'][:][np.newaxis,:,np.newaxis,np.newaxis]
        p = A * f.variables['P0'] + B * f.variables['PS'][:][:,np.newaxis,:,:]

        u = dynlib.utils.scale(f.variables['U'])[:,:,:,:]
        v = dynlib.utils.scale(f.variables['V'])[:,:,:,:]
        t = dynlib.utils.scale(f.variables['T'])[:,:,:,:]
        q = dynlib.utils.scale(f.variables['Q'])[:,:,:,:]
        z = dynlib.utils.scale(f.variables['Z3'])[:,:,:,:]
        slp_ = f.variables['PSL'][:,:,:]
        sp_ = f.variables['PS'][:,:,:]
        
        offset = tidx*tsperfile
        # No interpolation required
        if SLP:
            slp[offset:offset+tsperfile,:,:] = slp_
        if ORO:
            # TODO: Replacing actual orography with geopotential at the lowest model level for now
            oro[offset:offset+tsperfile,:,:] = z[:,-1,:,:]
            #oro[offset:offset+tsperfile,:,:] = (sp_[:,:,:] - slp_[:,:,:])*0.09 # from Pa to m

        for tplus in range(tsperfile):
            # Interpolate stuff onto 250 hPa
            pi = np.array([250.0e2, ])
            if UV_250:
                u_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(u[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                v_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(v[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
            if Z_250:
                z_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(z[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:] * 9.81  # Convert from m to m^2/s^2

            # Interpolate stuff onto 700 hPa
            pi = np.array([700.0e2, ])
            if TQ_700:
                t_700[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_700[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

            # Interpolate stuff onto 850 hPa
            pi = np.array([850.0e2, ])
            if UV_850:
                u_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(u[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                v_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(v[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
            if TQ_850:
                t_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

            # Interpolate stuff onto 925 hPa
            pi = np.array([925.0e2, ])
            if TQ_925:
                t_925[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_925[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

    grid.t_parsed = tsteps

    to_save = {}
    agg = {}
    if CALC_CYC:
        grid.oro = oro.mean(axis=0)
        mask, meta = detect_cyclones(slp, grid)
        mask = (mask > 0.5).astype('f8')
        
        dates_agg, __, agg['sfc','cyclone'], __, __ = dynlib.utils.aggregate(grid.t_parsed, mask, 'cal_month')
        to_save['sfc', 'cyclone'] = mask.mean(axis=0)

    if CALC_JETS:
        ja, jaoff = detect_jets(u_250, v_250, grid)
        
        jamask = dynlib.utils.normalize_lines(ja, jaoff, grid.dx, grid.dy)
        dates_agg, __, agg['250','jetaxis'], __, __ = dynlib.utils.aggregate(grid.t_parsed, jamask, 'cal_month')
        to_save['250', 'jetaxis'] = jamask.mean(axis=0)
    
    if CALC_RWB:
        rwb_a, rwb_c = detect_rwb(z_250, grid)

        dates_agg, __, agg['250','rwb_a'], __, __ = dynlib.utils.aggregate(grid.t_parsed, rwb_a, 'cal_month')
        dates_agg, __, agg['250','rwb_c'], __, __ = dynlib.utils.aggregate(grid.t_parsed, rwb_c, 'cal_month')
        to_save['250', 'rwb_a'] = rwb_a.mean(axis=0)
        to_save['250', 'rwb_c'] = rwb_c.mean(axis=0)

    if CALC_BLOCK:
        blockmask = detect_block(z_250, grid, tsperday).astype('f8')
        
        to_save['250', 'block'] = blockmask.mean(axis=0)

    if CALC_FRONTLINES:
        fronts, froff = detect_frontlines(t_850, q_850, u_850, v_850, grid)
        
        cold_fronts = dynlib.utils.normalize_lines(fronts[:,0,:,:], froff[:,0,:], grid.dx, grid.dy)
        warm_fronts = dynlib.utils.normalize_lines(fronts[:,1,:,:], froff[:,1,:], grid.dx, grid.dy)
        stat_fronts = dynlib.utils.normalize_lines(fronts[:,2,:,:], froff[:,2,:], grid.dx, grid.dy)

        dates_agg, __, agg['850','cold_front'], __, __ = dynlib.utils.aggregate(grid.t_parsed, cold_fronts, 'cal_month')
        dates_agg, __, agg['850','warm_front'], __, __ = dynlib.utils.aggregate(grid.t_parsed, warm_fronts, 'cal_month')
        dates_agg, __, agg['850','stat_front'], __, __ = dynlib.utils.aggregate(grid.t_parsed, stat_fronts, 'cal_month')
        to_save['850', 'cold_front'] = cold_fronts.mean(axis=0)
        to_save['850', 'warm_front'] = warm_fronts.mean(axis=0)
        to_save['850', 'stat_front'] = stat_fronts.mean(axis=0)

    if CALC_FRONTVOLUMES:
        frovo_id = detect_frontalvolumes(t_700, t_850, t_925, q_700, q_850, q_925, grid)
        
        frovo_id = frovo_id.astype('f8')
        frovo_id[frovo_id > 0.5] = 1.0
        for pidx, plev in enumerate(['700', '850', '925']):
            dates_agg, __, agg[plev,'frovo'], __, __ = dynlib.utils.aggregate(grid.t_parsed, frovo_id[:,pidx,:,:], 'cal_month')
            to_save[plev,'frovo'] = frovo_id[:,pidx,:,:].mean(axis=0)

    if CALC_EDDYVAR:
        dat_2_8, dat_8_ = calc_eddyvar(u_850, v_850, t_850, q_850, u_250, v_250, z_250, tsperday=tsperday)

        for plev, q in dat_2_8:
            l2h8 = dat_2_8[plev,q]
            l8 = dat_8_[plev,q]
            to_save[plev,q+'_l2h8'] = dat_2_8[plev,q].mean(axis=0)
            to_save[plev,q+'_l8'] = dat_8_[plev,q].mean(axis=0)
            l2h8 = to_save[plev,q+'_l2h8']
            l8 = to_save[plev,q+'_l8']

    save('multiyear_mean_noresm2mm_hist_1370-1374.nc', to_save, grid)
    save_time('multiyear_monthly_noresm2mm_hist_1370-1374.nc', dates_agg, agg, grid)


    print('Done for now.')

# C'est la fin.
