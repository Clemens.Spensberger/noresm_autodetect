#!/usr/bin/env python
# -*- encoding: utf8

'''
Determine detection thresholds to be used in the monthly analysis script

To determine reasonably accurate thresholds, use at least about 3 years of example 
model output. The more data the better with diminishing returns beyond about 15 
years. When using only few example years, make sure to not include exceptional 
climate states (e.g. a strong El Nino) in the example data to not bias detections.

On Fram this script requires the following modules
 - Python/3.6.4-intel-2018a
 - ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
 - netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

Initial development: 
Clemens Spensberger, August 2019
'''

import numpy as np
from cftime._cftime import DatetimeNoLeap as dt, timedelta as td

from dynlib.metio import metopen
from dynlib.settings_basic import conf

from dynlib import dynfor
import dynlib.diag
import dynlib.thermodyn
import dynlib.interpol


#basepath = '/tos-project4/NS9039K/shared/norcpm/cases/NorCPM'
basepath = '/Data/gfi/spengler/csp001/tmp_norcpm'
case = 'noresm1-cmip6_analysis_19500115'
member = '01'
output = 'h2'
tsperfile = 1
dates = [dt(2018,1,15) + td(1)*i for i in range(365)]
conf.file_std = '{case}/{case}_mem{member}/atm/hist/{case}_mem{member}.cam2.{output}.{yr:04d}-{month:02d}-{day:02d}-00000'
conf.file_std = '{case}_mem{member}.cam2.{output}.{yr:04d}-{month:02d}-{day:02d}-00000'
conf.gridsize = (96,144)

#basepath = '/Data/gfi/spengler/csp001/mis3b/interstadial'
#case = 'NBF1850_f19_tn11_test_mis3b_fwf3b_6hourly'
#case = 'NBF1850_f19_tn11_test_mis3b_6hourly'
#output = 'h1'
#tsperfile = 4
#member = None
#dates = [dt(1751,1,1) + td(1)*i for i in range(13717)]
#dates = [dt(1761,1,1) + td(1)*i for i in range(13924)]
#dates = [dt(1751,1,1) + td(1)*i for i in range(365)]
#conf.file_std = '{case}.cam2.{output}.{yr:04d}-{month:02d}-{day:02d}-21600'
#conf.gridsize = (96,144)

#basepath = '/Data/gfi/spengler/csp001/tmp_noresm2'
##case = 'N1850frc2_f09_tn14_20191012'
#case = 'NHISTfrc2_f09_tn14_20191025'
##case = 'NHIST_f19_tn14_20190710'
#output = 'h1'
#tsperfile = 73
#member = None
##dates = [dt(1350,1,2) + td(73)*i for i in range(150)]
#dates = [dt(1980,1,2) + td(73)*i for i in range(150)]
#conf.file_std = '{case}.cam.{output}.{yr:04d}-{month:02d}-{day:02d}-00000'
#conf.gridsize = (192,288)
##conf.gridsize = (96,144)

print(dates[0], dates[-1])

conf.datapath = [basepath, ]


# Thresholds for instantaneous diagnostics
CALC_JETS = True            # Jet axes after Spensberger et al. (2017)
CALC_FRONTLINES = True      # Front lines after Berry et al. (2011)
CALC_FRONTVOLUMES = True    # Frontal volumes after Spensberger and Sprenger (2018)


UV_250 = CALC_JETS
TQ_850 = CALC_FRONTVOLUMES | CALC_FRONTLINES


if __name__ == '__main__':
    s = (len(dates)*tsperfile,)+conf.gridsize

    if UV_250:
        u_250 = np.zeros(s)
        v_250 = np.zeros(s)
    if TQ_850:
        t_850 = np.zeros(s)
        q_850 = np.zeros(s)

    for tidx, date in enumerate(dates):
        if date.day == 1 or tsperfile >= 30:
            print(f'Reading {date}.')
        
        if member:
            f, grid = metopen(conf.file_std.format(case=case, member=member, output=output, 
                yr=date.year, month=date.month, day=date.day), quiet=True)
        else:
            f, grid = metopen(conf.file_std.format(case=case, output=output, 
                yr=date.year, month=date.month, day=date.day), quiet=True)

        grid.x = np.arange
    
        # Calculate pressure on hybrid sigma pressure levels
        A = f.variables['hyam'][:][np.newaxis,:,np.newaxis,np.newaxis]
        B = f.variables['hybm'][:][np.newaxis,:,np.newaxis,np.newaxis]
        p = A * f.variables['P0'] + B * f.variables['PS'][:][:,np.newaxis,:,:]

        u = f.variables['U'][:,:,:,:]
        v = f.variables['V'][:,:,:,:]
        t = f.variables['T'][:,:,:,:]
        q = f.variables['Q'][:,:,:,:]
        #z = f.variables['Z3'][:,:,:,:]
        
        for tplus in range(tsperfile):
            # Interpolate stuff onto 250 hPa
            pi = np.array([250.0e2, ])
            if UV_250:
                u_250[tidx*tsperfile+tplus,:,:] = dynlib.interpol.vert_by_coord(u[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                v_250[tidx*tsperfile+tplus,:,:] = dynlib.interpol.vert_by_coord(v[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

            # Interpolate stuff onto 850 hPa
            pi = np.array([850.0e2, ])
            if TQ_850:
                t_850[tidx*tsperfile+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_850[tidx*tsperfile+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
    
    # Recommend jet intensity threshold for jet detection
    if CALC_JETS:
        gs = dynlib.diag.grad_shear_nat(u_250, v_250, grid.dx, grid.dy)
        gs *= np.sqrt(u_250*u_250 + v_250*v_250)
        gs = sorted(gs[~np.isnan(gs)])
        idx = int(0.125 * len(gs))
        thres = -gs[idx]
        print(f'Recommended jetint_thres={thres:.3e}')

    # Recommend front intensity threshold for front volume and line detections.
    if CALC_FRONTVOLUMES | CALC_FRONTLINES:
        p_850 = np.ones(q_850.shape) * 850.0e2

        eqpt_850 = dynlib.thermodyn.thetae_from_q(q_850, t_850, p_850)
        tx, ty = dynlib.derivatives.grad(eqpt_850, grid.dx, grid.dy)
        t_grad = np.sqrt(tx*tx + ty*ty)
        t_grad = sorted(t_grad[~np.isnan(t_grad)])
        idx = int(0.875 * len(t_grad))
        thres = t_grad[idx]
        print(f'Recommended tfp_mingrad_largescale={thres:.3e}')

        eqpt_850 = dynlib.utils.smooth_xy_nan(eqpt_850, 5)
        tx, ty = dynlib.derivatives.grad(eqpt_850, grid.dx, grid.dy)
        t_grad = np.sqrt(tx*tx + ty*ty)
        t_grad = sorted(t_grad[~np.isnan(t_grad)])
        idx = int(0.95 * len(t_grad))
        thres = t_grad[idx]
        print(f'Recommended frint_thres={thres:.3e}')


    print('Done for now.')

# C'est la fin.
