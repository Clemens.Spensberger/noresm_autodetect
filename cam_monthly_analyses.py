#!/usr/bin/env python
# -*- encoding: utf8

'''
Automatic weather feature detections and calculation of storm track diagnostics 

Developed for use with NorESM/NorCPM to make weather features and storm track diagnostics 
available also for simulations where storing high-temporal resolution output is not feasible.
The script and procedure might be easily transferable to any coupled model using CAM as its 
atmospheric component.

The atmospheric feature detections are tuned to give reasonable results for instantaneous 
output on a global 144 x 96 grid, corresponding to 2.5 x 1.9 degree resolution.

Feature detections and diagnostics calculations are based on ``dynlib'', a library of tools
developed in the atmospheric dynamics work group at the Geophysical Institute, University of 
Bergen. 

Output is expected as daily means or 6 hourly instantaneous values, one file per output time
step. Call the script separately for each ensemble member.

On Fram this script requires the following modules
 - Python/3.6.4-intel-2018a
 - ScaLAPACK/2.0.2-gompi-2018a-OpenBLAS-0.2.20
 - netcdf4-python/1.4.0-intel-2018a-Python-3.6.4

Initial development: 
Clemens Spensberger, August 2019
'''

import numpy as np
from cftime._cftime import DatetimeNoLeap as dt, timedelta as td

from dynlib.metio.generic import metopen, conf

import dynlib.interpol
import dynlib.utils

from cam_detection_functions import *


#basepath = '/tos-project4/NS9039K/shared/norcpm/cases/NorCPM'
basepath = '/Data/gfi/spengler/csp001/tmp_norcpm'
case = 'noresm1-cmip6_analysis_19500115'
member = '01'
output = 'h2'
yr, month = 2018, 6

tsperday = 4
tsperfile = 4
conf.datapath = [basepath, ]
#fname_pattern = '{case}/{case}_mem{member}/atm/hist/{case}_mem{member}.cam2.{output}.{yr:04d}-{month:02d}-{day:02d}-21600'
fname_pattern = '{case}_mem{member}.cam2.{output}.{yr:04d}-{month:02d}-{day:02d}-21600'
conf.gridsize = (96,144)

# No leap years calendar used in NorESM/NorCPM
DAYS_PER_MONTH = {1: 31, 2: 28, 3: 31, 4: 30, 5: 31, 6: 30, 7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}


# Instantaneous diagnostics
CALC_CYC = True             # Cyclone identification after Wernli and Schwierz (2006)
CALC_JETS = True            # Jet axes after Spensberger et al. (2017)
CALC_RWB = True             # Rossby wave breaking after Rivière (2007)
CALC_FRONTLINES = True      # Front lines after Jenkner et al. (2010), Schemm et al. (2015)
CALC_FRONTVOLUMES = True    # Frontal volumes after Spensberger and Sprenger (2018)
CALC_EDDYVAR = True         # Eddy covariances u'v', v'T', u'u', v'v' and z'z' 
                            #    on 2-8 and 8-30 day windows (see Chang et al. 2002)

# Diagnostics requiring tracking in time 
CALC_BLOCK = True           # Atmospheric blocks after Masato et al. (2014)


UV_250 = CALC_JETS | CALC_EDDYVAR
Z_250 = CALC_RWB | CALC_EDDYVAR | CALC_BLOCK
TQ_700 = CALC_FRONTVOLUMES
UV_850 = CALC_FRONTLINES | CALC_EDDYVAR
TQ_850 = CALC_FRONTVOLUMES | CALC_FRONTLINES | CALC_EDDYVAR
TQ_925 = CALC_FRONTVOLUMES
SLP = CALC_CYC
ORO = CALC_CYC


if __name__ == '__main__':
    # Days from the preceeding month to be read in as well. Required to be able to track features 
    # with minimum duration constraints across month boundaries. Set to zero where preceeding data is
    # unavailable
    prevdays = 5

    days = DAYS_PER_MONTH[month]
    dates = [dt(yr,month,1)-td(prevdays) + td(tsperfile/tsperday) * i for i in range((days+prevdays)*tsperday//tsperfile)]

    # Mask dates belonging to current month rather than the preceeding 5 
    dates_ts = [dt(yr,month,1)-td(prevdays) + td(1.0/tsperday) * i for i in range((days+prevdays)*tsperday)]
    curmonth = np.array([date.month == month for date in dates_ts], dtype='bool')

    s = (len(dates)*tsperfile,)+conf.gridsize

    if UV_250:
        u_250 = np.zeros(s)
        v_250 = np.zeros(s)
    if Z_250:
        z_250 = np.zeros(s)
    if TQ_700:
        t_700 = np.zeros(s)
        q_700 = np.zeros(s)
    if UV_850:
        u_850 = np.zeros(s)
        v_850 = np.zeros(s)
    if TQ_850:
        t_850 = np.zeros(s)
        q_850 = np.zeros(s)
    if TQ_925:
        t_925 = np.zeros(s)
        q_925 = np.zeros(s)
    if SLP:
        slp = np.zeros(s)
    if ORO:
        oro = np.zeros(s)

    tsteps = []
    for tidx, date in enumerate(dates):
        f, grid = metopen(fname_pattern.format(case=case, member=member, output=output, 
            yr=date.year, month=date.month, day=date.day))

        tsteps.extend(grid.t_parsed)

        # Calculate pressure on hybrid sigma pressure levels
        A = f.variables['hyam'][:][np.newaxis,:,np.newaxis,np.newaxis]
        B = f.variables['hybm'][:][np.newaxis,:,np.newaxis,np.newaxis]
        p = A * f.variables['P0'] + B * f.variables['PS'][:][:,np.newaxis,:,:]

        u = dynlib.utils.scale(f.variables['U'])[:,:,:,:]
        v = dynlib.utils.scale(f.variables['V'])[:,:,:,:]
        t = dynlib.utils.scale(f.variables['T'])[:,:,:,:]
        q = dynlib.utils.scale(f.variables['Q'])[:,:,:,:]
        z = dynlib.utils.scale(f.variables['Z3'])[:,:,:,:]
        slp_ = f.variables['PSL'][:,:,:]

        offset = tidx*tsperfile
        # No interpolation required
        if SLP:
            slp[offset:offset+tsperfile,:,:] = slp_
        if ORO:
            # TODO: Replacing actual orography with geopotential at the lowest model level for now
            oro[offset:offset+tsperfile,:,:] = z[:,-1,:,:]
        
        for tplus in range(tsperfile):
            # Interpolate stuff onto 250 hPa
            pi = np.array([250.0e2, ])
            if UV_250:
                u_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(u[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                v_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(v[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
            if Z_250:
                z_250[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(z[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:] * 9.81  # Convert from m to m^2/s^2

            # Interpolate stuff onto 700 hPa
            pi = np.array([700.0e2, ])
            if TQ_700:
                t_700[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_700[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

            # Interpolate stuff onto 850 hPa
            pi = np.array([850.0e2, ])
            if UV_850:
                u_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(u[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                v_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(v[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
            if TQ_850:
                t_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_850[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

            # Interpolate stuff onto 925 hPa
            pi = np.array([925.0e2, ])
            if TQ_925:
                t_925[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(t[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]
                q_925[offset+tplus,:,:] = dynlib.interpol.vert_by_coord(q[tplus,:,:,:], p[tplus,:,:,:], pi)[0,:,:]

    grid.t_parsed = tsteps
    
    to_save = {}
    if CALC_CYC:
        grid.oro = oro.mean(axis=0)
        mask, meta = detect_cyclones(slp[curmonth,:,:], grid)

        to_save['sfc', 'cyclone'] = (mask > 0.5).astype('f8').mean(axis=0)

    if CALC_JETS:
        ja, jaoff = detect_jets(u_250[curmonth,:,:], v_250[curmonth,:,:], grid)
        
        jamask = dynlib.utils.normalize_lines(ja, jaoff, grid.dx, grid.dy)
        to_save['250', 'jetaxis'] = jamask.mean(axis=0)
    
    if CALC_RWB:
        rwb_a, rwb_c = detect_rwb(z_250[curmonth,:,:], grid)

        to_save['250', 'rwb_a'] = rwb_a.mean(axis=0)
        to_save['250', 'rwb_c'] = rwb_c.mean(axis=0)

    if CALC_BLOCK:
        blockmask = detect_block(z_250, grid, tsperday).astype('f8')
        
        # Note: Restriction to curmonth only AFTER the detection.
        to_save['250', 'block'] = blockmask[curmonth,:,:].mean(axis=0)

    if CALC_FRONTLINES:
        fronts, froff = detect_frontlines(t_850[curmonth,:,:], q_850[curmonth,:,:], u_850[curmonth,:,:], v_850[curmonth,:,:], grid)

        to_save['850', 'cold_front'] = dynlib.utils.normalize_lines(fronts[:,0,:,:], froff[:,0,:], grid.dy, grid.dy).mean(axis=0)
        to_save['850', 'warm_front'] = dynlib.utils.normalize_lines(fronts[:,1,:,:], froff[:,1,:], grid.dy, grid.dy).mean(axis=0)
        to_save['850', 'stat_front'] = dynlib.utils.normalize_lines(fronts[:,2,:,:], froff[:,2,:], grid.dy, grid.dy).mean(axis=0)

    if CALC_FRONTVOLUMES:
        frovo_id = detect_frontalvolumes(t_700[curmonth,:,:], t_850[curmonth,:,:], t_925[curmonth,:,:], 
                q_700[curmonth,:,:], q_850[curmonth,:,:], q_925[curmonth,:,:], grid)
        
        frovo_id = frovo_id.astype('f8')
        frovo_id[frovo_id > 0.5] = 1.0
        for pidx, plev in enumerate(['700', '850', '925']):
            to_save[plev,'frovo'] = frovo_id[:,pidx,:,:].mean(axis=0)

    if CALC_EDDYVAR:
        dat_2_8, dat_8_ = calc_eddyvar(
                u_850[curmonth,:,:], v_850[curmonth,:,:], t_850[curmonth,:,:], q_850[curmonth,:,:], 
                u_250[curmonth,:,:], v_250[curmonth,:,:], z_250[curmonth,:,:], tsperday=tsperday)

        for plev, q in dat_2_8:
            l2h8 = dat_2_8[plev,q]
            l8 = dat_8_[plev,q]
            to_save[plev,q+'_l2h8'] = dat_2_8[plev,q].mean(axis=0)
            to_save[plev,q+'_l8'] = dat_8_[plev,q].mean(axis=0)
            l2h8 = to_save[plev,q+'_l2h8']
            l8 = to_save[plev,q+'_l8']

    save(f'{case}_mem{member}.dynlib.mon_feats.{yr:04d}_{month:02d}.nc', to_save, grid)

    print('Done for now.')

# C'est la fin.
